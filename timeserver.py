import datetime
import socket
import sys
from _thread import start_new_thread

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.setblocking(False)
server.bind(("", 1303))
server.listen(1000)


def new_connection(connection, address):
    curr_time = datetime.datetime.today().strftime("%d.%m.%Y %H:%M")
    connection.send(str.encode(curr_time))


try:
    while True:
        try:
            conn, addr = server.accept()
        except Exception:
            pass
        else:
            print(f"new connection: {addr}")
            start_new_thread(new_connection, (conn, addr))
except KeyboardInterrupt:
    sys.exit()

