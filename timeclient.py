import socket
import sys

client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

user_ip = input("Ваш ip-адрес: ")

try:
    client.connect((user_ip, 1303))
except socket.error as e:
    print(e)


msg = client.recv(1024).decode("utf-8")
print(msg)

client.close()
